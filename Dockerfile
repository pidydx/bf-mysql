#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=mysql
ENV APP_GROUP=mysql

# Set version pins
#ENV VERSION

# Set base dependencies
ENV BASE_DEPS ca-certificates mariadb-server


# Set build dependencies
#ENV BUILD_DEPS build-essential

# Create app user and group
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -r -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_DEPS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
#FROM base AS builder
#
#RUN apt-get update -q \
# && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}
#
## Run build
#COPY build.sh /usr/src/build.sh
#WORKDIR /usr/src
#RUN ./build.sh

##########################
# Create final container #
##########################
FROM base

# Finalize install
COPY etc/ /etc/
#COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN update-ca-certificates \
 && rm /etc/mysql/mariadb.conf.d/50-* \
 && rm -Rf /var/lib/mysql/* \
 && mkdir -p /var/run/mysqld \
 && chown ${APP_USER}:${APP_GROUP} /var/run/mysqld

EXPOSE 3306/tcp
VOLUME ["/etc/mysql", "/var/lib/mysql"]
USER $APP_USER

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["mysql"]
