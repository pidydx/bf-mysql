#!/bin/bash

set -e

if [ "$1" = 'init' ]; then
    if [ ! -d /var/lib/mysql/data ]; then
        mysql_install_db --user=mysql --datadir=/var/lib/mysql/data --auth-root-authentication-method=socket
        mysqld --datadir=/var/lib/mysql/data --user=mysql --bootstrap < /etc/mysql/setup.sql
        mysqld --datadir=/var/lib/mysql/data --user=mysql --bootstrap < /etc/mysql/init.sql
        exec echo "Database created."
    fi
    mysqld --datadir=/var/lib/mysql/data --user=mysql --bootstrap < /etc/mysql/update.sql
    exec echo "Initialization complete."
fi

if [ "$1" = 'mysql' ]; then
    exec mysqld --console --datadir=/var/lib/mysql/data --user=mysql --pid-file=/var/run/mysqld/mysqld.pid
fi

exec "$@"